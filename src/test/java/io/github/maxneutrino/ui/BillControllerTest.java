package io.github.maxneutrino.ui;

import io.github.maxneutrino.repository.ChargeRateInMemoryRepositoryImpl;
import io.github.maxneutrino.repository.ChargeRateRepository;
import io.github.maxneutrino.service.*;
import io.github.maxneutrino.service.converter.BillToRawStringConverterImpl;
import io.github.maxneutrino.service.converter.BillToStringConverter;
import io.github.maxneutrino.service.converter.TravelTimeInputConverter;
import io.github.maxneutrino.service.converter.TravelTimeInputConverterImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Sorry that I have only one test which covers all functionality
 * I never do a such thing in my job and allowed this just because this is test task and I want to sleep at midnight
 * Hope for your understanding
 */
public class BillControllerTest {

    private BillController controller;

    @Before
    public void setUp() throws Exception {
        TravelTimeInputConverter travelTimeInputConverter = new TravelTimeInputConverterImpl();
        BillToStringConverter billToStringConverter = new BillToRawStringConverterImpl();

        ChargeRateRepository chargeRateRepository = new ChargeRateInMemoryRepositoryImpl();

        List<VehicleTypeResolver> vehicleTypeResolvers = new ArrayList<>();
        vehicleTypeResolvers.add(new BikeTypeResolver());
        vehicleTypeResolvers.add(new CarTypeResolver());


        VehicleService vehicleService = new VehicleServiceImpl(vehicleTypeResolvers);
        VehicleChargeService vehicleChargeService = new VehicleChargeServiceImpl(chargeRateRepository, vehicleService);

        controller = new BillController(travelTimeInputConverter, billToStringConverter, vehicleChargeService);
    }

    @Test
    public void calculateBill_carOneDayTest() {
        String expected = "Charge for 0h 28m (AM rate): £0.93\n" +
                "Charge for 2h 42m (PM rate): £6.75\n" +
                "Total Charge: £7.68";

        String actual = controller.calculateBill("Car: 24/04/2008 11:32 - 24/04/2008 14:42");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void calculateBill_bikeOneDayTest() {
        String expected = "Charge for 0h 0m (AM rate): £0.00\n" +
                "Charge for 2h 0m (PM rate): £2.00\n" +
                "Total Charge: £2.00";

        String actual = controller.calculateBill("Motorbike: 24/04/2008 17:00 - 24/04/2008 22:11");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void calculateBill_vanFourDaysTest() {
        String expected = "Charge for 13h 39m (AM rate): £27.30\n" +
                "Charge for 21h 0m (PM rate): £52.50\n" +
                "Total Charge: £79.80";

        String actual = controller.calculateBill("Van: 25/04/2008 10:23 - 28/04/2008 09:02");
        Assert.assertEquals(expected, actual);
    }
}

package io.github.maxneutrino.service;

import io.github.maxneutrino.model.VehicleType;

import java.util.Optional;

public interface VehicleTypeResolver {

    Optional<VehicleType> resolveByName(String name);
}

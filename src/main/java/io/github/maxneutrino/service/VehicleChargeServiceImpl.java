package io.github.maxneutrino.service;

import io.github.maxneutrino.model.*;
import io.github.maxneutrino.repository.ChargeRateRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class VehicleChargeServiceImpl implements VehicleChargeService {

    
    private final ChargeRateRepository chargeRateRepository;
    
    private final VehicleService vehicleService;

    public VehicleChargeServiceImpl(ChargeRateRepository chargeRateRepository, VehicleService vehicleService) {
        this.chargeRateRepository = chargeRateRepository;
        this.vehicleService = vehicleService;
    }

    @Override
    public Bill calculate(TravelTime request) {
        Vehicle vehicle = vehicleService.findByName(request.getVehicle());
        List<ChargeRate> rates = chargeRateRepository.findAllByVehicleType(vehicle.getType());
        return calculate(rates, request.getStart(), request.getEnd());
    }

    private Bill calculate(List<ChargeRate> rates, LocalDateTime start, LocalDateTime end) {
        rates.sort(Comparator.comparing(ChargeRate::getStartTime));

        List<BillEntry> billEntries = new ArrayList<>();

        for (ChargeRate rate : rates) {
            LocalDateTime startChargeDateTime = LocalDateTime.of(start.toLocalDate(), rate.getStartTime());
            LocalDateTime endChargeDateTime = LocalDateTime.of(start.toLocalDate(), rate.getEndTime());

            long chargeMinutes = 0;

            while (startChargeDateTime.isBefore(end) && endChargeDateTime.isAfter(start)) {
                LocalDateTime toChargeStart;
                LocalDateTime toChargeEnd;

                if (startChargeDateTime.isBefore(start)) {
                    toChargeStart = start;
                } else {
                    toChargeStart = startChargeDateTime;
                }

                if (endChargeDateTime.isBefore(end)) {
                    toChargeEnd = endChargeDateTime;
                } else {
                    toChargeEnd = end;
                }

                chargeMinutes += ChronoUnit.MINUTES.between(toChargeStart, toChargeEnd);
                startChargeDateTime = startChargeDateTime.plusDays(1);
                endChargeDateTime = endChargeDateTime.plusDays(1);
            }

            double chargeAmount = chargeMinutes * (rate.getAmount() / 60);
            long hours = chargeMinutes / 60;
            long minutes = chargeMinutes % 60;

            billEntries.add(new BillEntry(rate.getName(), hours, minutes, chargeAmount));
        }

        double total = findTotal(billEntries);

        return new Bill(billEntries, total);
    }

    private double findTotal(List<BillEntry> billEntries) {
        double total = 0.0;

        for (BillEntry billEntry : billEntries) {
            total += billEntry.getAmount();
        }

        return total;
    }
}

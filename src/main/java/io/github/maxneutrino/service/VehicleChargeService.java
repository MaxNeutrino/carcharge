package io.github.maxneutrino.service;

import io.github.maxneutrino.model.Bill;
import io.github.maxneutrino.model.TravelTime;

public interface VehicleChargeService {

    Bill calculate(TravelTime request);
}

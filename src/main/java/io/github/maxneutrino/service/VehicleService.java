package io.github.maxneutrino.service;

import io.github.maxneutrino.model.Vehicle;

public interface VehicleService {

    Vehicle findByName(String name);
}

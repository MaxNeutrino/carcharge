package io.github.maxneutrino.service.converter;

import io.github.maxneutrino.model.TravelTime;

public interface TravelTimeInputConverter {

    TravelTime convert(String input);
}

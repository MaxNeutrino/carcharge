package io.github.maxneutrino.service.converter;

import io.github.maxneutrino.model.Bill;

public interface BillToStringConverter {

    String convert(Bill bill);
}

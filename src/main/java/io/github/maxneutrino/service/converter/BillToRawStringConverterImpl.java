package io.github.maxneutrino.service.converter;

import io.github.maxneutrino.model.Bill;
import io.github.maxneutrino.model.BillEntry;

public class BillToRawStringConverterImpl implements BillToStringConverter {

    /**
     * @return string with format
     * Charge for {n}h {r}m ({rate name}): £{amount}
     * Total charge: £{n}
     */
    @Override
    public String convert(Bill bill) {
        StringBuilder stringBuilder = new StringBuilder();

        for (BillEntry entry : bill.getEntries()) {
            String entryMessage = String.format("Charge for %dh %dm (%s): £%.2f\n",
                    entry.getHours(), entry.getMinutes(), entry.getTitle(), entry.getAmount());

            stringBuilder.append(entryMessage);
        }

        String totalMessage = String.format("Total Charge: £%.2f", bill.getTotal());
        stringBuilder.append(totalMessage);

        return stringBuilder.toString();
    }
}

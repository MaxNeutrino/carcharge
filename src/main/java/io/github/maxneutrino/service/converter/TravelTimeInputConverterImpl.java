package io.github.maxneutrino.service.converter;

import io.github.maxneutrino.model.TravelTime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TravelTimeInputConverterImpl implements TravelTimeInputConverter {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    /**
     * Supports message with format: {Vehicle}: dd/MM/yyyy HH:mm - dd/MM/yyyy HH:mm
     */
    @Override
    public TravelTime convert(String input) {
        int doubleDotIndex = input.indexOf(":");
        String vehicleName = input.substring(0, doubleDotIndex);

        int hyphenIndex = input.indexOf("-");
        String startTimeRaw = input.substring(doubleDotIndex + 1, hyphenIndex).trim();

        String endTimeRaw = input.substring(hyphenIndex + 1).trim();

        LocalDateTime start = LocalDateTime.from(formatter.parse(startTimeRaw));
        LocalDateTime end = LocalDateTime.from(formatter.parse(endTimeRaw));

        return new TravelTime(vehicleName, start, end);
    }
}

package io.github.maxneutrino.service;

public class ConsoleMessagePrinterService implements MessagePrinterService {

    @Override
    public void print(String message) {
        System.out.println(message);
    }
}

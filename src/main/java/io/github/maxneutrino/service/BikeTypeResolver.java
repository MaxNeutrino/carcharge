package io.github.maxneutrino.service;

import io.github.maxneutrino.model.VehicleType;

import java.util.Optional;

public class BikeTypeResolver implements VehicleTypeResolver {

    @Override
    public Optional<VehicleType> resolveByName(String name) {
        if (name.equals("Motorbike"))
            return Optional.of(VehicleType.BIKE);

        return Optional.empty();
    }
}

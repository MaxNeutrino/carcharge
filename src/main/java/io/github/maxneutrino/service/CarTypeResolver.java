package io.github.maxneutrino.service;

import io.github.maxneutrino.model.VehicleType;

import java.util.Optional;

public class CarTypeResolver implements VehicleTypeResolver {

    @Override
    public Optional<VehicleType> resolveByName(String name) {
        if (name.equals("Car") || name.equals("Van"))
            return Optional.of(VehicleType.CAR);

        return Optional.empty();
    }
}

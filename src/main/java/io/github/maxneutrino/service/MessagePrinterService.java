package io.github.maxneutrino.service;

public interface MessagePrinterService {

    void print(String message);
}

package io.github.maxneutrino.service;

import io.github.maxneutrino.model.Vehicle;
import io.github.maxneutrino.model.VehicleType;

import java.util.List;
import java.util.Optional;

public class VehicleServiceImpl implements VehicleService {

    private final List<VehicleTypeResolver> vehicleTypeResolvers;

    public VehicleServiceImpl(List<VehicleTypeResolver> vehicleTypeResolvers) {
        this.vehicleTypeResolvers = vehicleTypeResolvers;
    }

    @Override
    public Vehicle findByName(String name) {
        VehicleType vehicleType = resolveByName(name);
        return new Vehicle(name, vehicleType);
    }

    private VehicleType resolveByName(String name) {
        for (VehicleTypeResolver resolver : vehicleTypeResolvers) {
            Optional<VehicleType> optionalVehicleType = resolver.resolveByName(name);
            if (optionalVehicleType.isPresent())
                return optionalVehicleType.get();
        }

        throw new IllegalArgumentException("Vehicle with name " + name + " not found");
    }
}

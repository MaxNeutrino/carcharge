package io.github.maxneutrino.model;

import java.util.Objects;

public class BillEntry {

    private String title;

    private long hours;

    private long minutes;

    private double amount;

    public BillEntry(String title, long hours, long minutes, double amount) {
        this.title = title;
        this.hours = hours;
        this.minutes = minutes;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BillEntry billEntry = (BillEntry) o;
        return hours == billEntry.hours &&
                minutes == billEntry.minutes &&
                Double.compare(billEntry.amount, amount) == 0 &&
                Objects.equals(title, billEntry.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, hours, minutes, amount);
    }

    @Override
    public String toString() {
        return "BillEntry{" +
                "title='" + title + '\'' +
                ", hours=" + hours +
                ", minutes=" + minutes +
                ", amount=" + amount +
                '}';
    }
}

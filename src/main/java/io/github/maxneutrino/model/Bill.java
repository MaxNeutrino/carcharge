package io.github.maxneutrino.model;

import java.util.List;
import java.util.Objects;

public class Bill {

    private List<BillEntry> entries;

    private double total;

    public Bill(List<BillEntry> entries, double total) {
        this.entries = entries;
        this.total = total;
    }

    public List<BillEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<BillEntry> entries) {
        this.entries = entries;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bill bill = (Bill) o;
        return Double.compare(bill.total, total) == 0 &&
                Objects.equals(entries, bill.entries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entries, total);
    }

    @Override
    public String toString() {
        return "Bill{" +
                "entries=" + entries +
                ", total=" + total +
                '}';
    }
}

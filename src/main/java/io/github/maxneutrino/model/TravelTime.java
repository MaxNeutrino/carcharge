package io.github.maxneutrino.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class TravelTime {

    private String vehicle;

    private LocalDateTime start;

    private LocalDateTime end;

    public TravelTime(String vehicle, LocalDateTime start, LocalDateTime end) {
        this.vehicle = vehicle;
        this.start = start;
        this.end = end;
    }

    public String getVehicle() {
        return vehicle;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TravelTime that = (TravelTime) o;
        return Objects.equals(vehicle, that.vehicle) &&
                Objects.equals(start, that.start) &&
                Objects.equals(end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicle, start, end);
    }

    @Override
    public String toString() {
        return "BillRequest{" +
                "vehicle='" + vehicle + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}

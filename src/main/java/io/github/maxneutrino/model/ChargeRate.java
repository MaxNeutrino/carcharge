package io.github.maxneutrino.model;

import java.time.LocalTime;
import java.util.Objects;

public class ChargeRate {

    private String name;

    private LocalTime startTime;

    private LocalTime endTime;

    private double amount;

    private VehicleType vehicleType;

    public ChargeRate(String name, LocalTime startTime, LocalTime endTime, double amount, VehicleType vehicleType) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amount = amount;
        this.vehicleType = vehicleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargeRate that = (ChargeRate) o;
        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(name, that.name) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                vehicleType == that.vehicleType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, startTime, endTime, amount, vehicleType);
    }

    @Override
    public String toString() {
        return "ChargeRate{" +
                "name='" + name + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", amount=" + amount +
                ", vehicleType=" + vehicleType +
                '}';
    }
}

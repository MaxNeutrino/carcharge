package io.github.maxneutrino.model;

public enum  VehicleType {
    CAR,
    BIKE;
}

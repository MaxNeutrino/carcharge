package io.github.maxneutrino.ui;

import io.github.maxneutrino.model.Bill;
import io.github.maxneutrino.model.TravelTime;
import io.github.maxneutrino.service.VehicleChargeService;
import io.github.maxneutrino.service.converter.BillToStringConverter;
import io.github.maxneutrino.service.converter.TravelTimeInputConverter;

public class BillController {


    private final TravelTimeInputConverter travelTimeInputConverter;

    private final BillToStringConverter billToStringConverter;

    private final VehicleChargeService vehicleChargeService;

    public BillController(TravelTimeInputConverter travelTimeInputConverter,
                          BillToStringConverter billToStringConverter,
                          VehicleChargeService vehicleChargeService) {
        this.travelTimeInputConverter = travelTimeInputConverter;
        this.billToStringConverter = billToStringConverter;
        this.vehicleChargeService = vehicleChargeService;
    }

    public String calculateBill(String input) {
        TravelTime travelTime = travelTimeInputConverter.convert(input);
        Bill bill = vehicleChargeService.calculate(travelTime);
        return billToStringConverter.convert(bill);
    }
}

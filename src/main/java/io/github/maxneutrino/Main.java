package io.github.maxneutrino;

import io.github.maxneutrino.repository.ChargeRateInMemoryRepositoryImpl;
import io.github.maxneutrino.repository.ChargeRateRepository;
import io.github.maxneutrino.service.*;
import io.github.maxneutrino.service.converter.BillToRawStringConverterImpl;
import io.github.maxneutrino.service.converter.BillToStringConverter;
import io.github.maxneutrino.service.converter.TravelTimeInputConverter;
import io.github.maxneutrino.service.converter.TravelTimeInputConverterImpl;
import io.github.maxneutrino.ui.BillController;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * Application supports only single format of arg: <br/>
     * {Vehicle}: dd/MM/yyyy HH:mm - dd/MM/yyyy HH:mm <br/>
     * <br/>
     * Example: Car: 24/04/2008 11:32 - 24/04/2008 14:42
     *
     * Sorry that I did not provide any context like it realised in spring with factories and lot of funny stuff
     * I never do a such thing in my job and allowed this just because this is test task and I want to sleep at midnight
     * Hope for your understanding
     */
    public static void main(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Invalid length of arguments, expected 1");

        String input = args[0];

        TravelTimeInputConverter travelTimeInputConverter = new TravelTimeInputConverterImpl();
        BillToStringConverter billToStringConverter = new BillToRawStringConverterImpl();

        ChargeRateRepository chargeRateRepository = new ChargeRateInMemoryRepositoryImpl();

        List<VehicleTypeResolver> vehicleTypeResolvers = new ArrayList<>();
        vehicleTypeResolvers.add(new BikeTypeResolver());
        vehicleTypeResolvers.add(new CarTypeResolver());


        VehicleService vehicleService = new VehicleServiceImpl(vehicleTypeResolvers);
        VehicleChargeService vehicleChargeService = new VehicleChargeServiceImpl(chargeRateRepository, vehicleService);

        BillController controller = new BillController(travelTimeInputConverter, billToStringConverter, vehicleChargeService);

        MessagePrinterService printerService = new ConsoleMessagePrinterService();

        String bill = controller.calculateBill(input);
        printerService.print(bill);
    }
}

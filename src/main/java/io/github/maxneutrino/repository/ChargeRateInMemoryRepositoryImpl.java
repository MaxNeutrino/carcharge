package io.github.maxneutrino.repository;

import io.github.maxneutrino.model.ChargeRate;
import io.github.maxneutrino.model.VehicleType;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Emulation of real repository
 * Usually used some SQL or NoSQL database for this purpose
 */
public class ChargeRateInMemoryRepositoryImpl implements ChargeRateRepository {

    private List<ChargeRate> inMemoryRates;

    public ChargeRateInMemoryRepositoryImpl() {
        inMemoryRates = new ArrayList<>();

        inMemoryRates.add(new ChargeRate(
                "AM rate",
                LocalTime.of(7, 0),
                LocalTime.of(12, 0),
                2,
                VehicleType.CAR
        ));

        inMemoryRates.add(new ChargeRate(
                "PM rate",
                LocalTime.of(12, 0),
                LocalTime.of(19, 0),
                2.5,
                VehicleType.CAR
        ));

        inMemoryRates.add(new ChargeRate(
                "AM rate",
                LocalTime.of(7, 0),
                LocalTime.of(12, 0),
                1,
                VehicleType.BIKE
        ));

        inMemoryRates.add(new ChargeRate(
                "PM rate",
                LocalTime.of(12, 0),
                LocalTime.of(19, 0),
                1,
                VehicleType.BIKE
        ));
    }

    @Override
    public List<ChargeRate> findAllByVehicleType(VehicleType type) {
        return inMemoryRates.stream()
                .filter(rate -> rate.getVehicleType().equals(type))
                .collect(Collectors.toList());
    }
}

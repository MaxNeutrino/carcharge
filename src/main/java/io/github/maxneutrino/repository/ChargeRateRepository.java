package io.github.maxneutrino.repository;

import io.github.maxneutrino.model.ChargeRate;
import io.github.maxneutrino.model.VehicleType;

import java.util.List;
import java.util.Optional;

public interface ChargeRateRepository {

    List<ChargeRate> findAllByVehicleType(VehicleType type);
}
